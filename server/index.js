/* eslint consistent-return:0 */

const express = require('express');
const logger = require('./logger');
const bodyParser = require("body-parser");
const path = require('path');

const router = express.Router;
const server = express();


server.use(bodyParser.urlencoded({ extended: true }));
server.use(bodyParser.json());

const cookieParser = require("cookie-parser");

server.use(cookieParser());

/**
 * Products API
 * GET `/products` - Get all products
 * GET `/products/:id` - Get product by id
 */

const products = require("./products");

const productsRouter = router();

// GET all products
productsRouter.get("/", (req, res) => res.json(products));

// GET product by id
productsRouter.get("/:id", (req, res, next) => {
  const id = req.params.id;
  if (id in products) {
    res.json(products[id]);
  }
  return next(new Error("Product doesn\'t exist"));
});

server.use("/api/products", productsRouter);

/**
 * Cart API
 * GET `/cart` - Get cart items with summary
 * DELETE `/cart` - Empty cart
 * POST `/cart/:id` `quantity?: number` - Add `quantity` to item with `id`
 * PUT `/cart/:id` `quantity: number` - Update `quantity` to item with `id`
 * DELETE `/cart/:id` - Remove item with `id`
 */

const Cart = require("./cart");

const carts = new Map();
let nextCartId = 1;

const cartRouter = router();

const cartRequest = function (action) {
  return (req, res, next) => {
    let cart;

    const cartId = +req.cookies.cart_id;
    if (cartId && carts.has(cartId)) {
      // Get existing cart
      cart = carts.get(cartId);
    } else {
      // Create new cart
      cart = new Cart();
      carts.set(nextCartId, cart);
      res.cookie("cart_id", nextCartId);
      nextCartId++;
    }

    const data = action(cart, req, res);
    if (data instanceof Error) {
      return next(data);
    }
    return res.json(data.get());
  };
};

// GET cart
cartRouter.get("/", cartRequest((c) => c));

// DELETE (empty) cart
cartRouter.delete("/", cartRequest((c) => c.clear()));

// POST (create or add) quantity to item by id
cartRouter.post("/:id", cartRequest((c, req) => c.add(+req.params.id, +req.body.quantity || 1)));

// PUT (update) quantity to item by id
cartRouter.put("/:id", cartRequest((c, req) => c.update(+req.params.id, +req.body.quantity)));

// DELETE quantity to item by id
cartRouter.delete("/:id", cartRequest((c, req) => c.remove(+req.params.id)));

server.use("/api/cart", cartRouter);

// images
server.use('/api/images', express.static(path.join(__dirname, 'images')))

// Error handling
//server.use((req, res, next) => next(new Error("Request not found")));

const errorBadRequest = 400;
server.use((error, req, res, next) => res.status(errorBadRequest).json({ error: error.message }));


const argv = require('minimist')(process.argv.slice(2));
const setup = require('./middlewares/frontendMiddleware');
const isDev = process.env.NODE_ENV !== 'production';
const ngrok = (isDev && process.env.ENABLE_TUNNEL) || argv.tunnel ? require('ngrok') : false;
const resolve = require('path').resolve;
//const app = express();

// If you need a backend, e.g. an API, add your custom backend-specific middleware here
// app.use('/api', myApi);

// In production we need to pass these values in instead of relying on webpack
setup(server, {
  outputPath: resolve(process.cwd(), 'build'),
  publicPath: '/',
});

// get the intended host and port number, use localhost and port 3000 if not provided
const customHost = argv.host || process.env.HOST;
const host = customHost || null; // Let http.Server use its default IPv6/4 host
const prettyHost = customHost || 'localhost';

// Change port by running `$ SERVER_PORT=XXXX npm start`

const port = argv.port || process.env.PORT || 3000;

// Start your server.
server.listen(port, host, (err) => {
  if (err) {
    return logger.error(err.message);
  }

  // Connect to ngrok in dev mode
  if (ngrok) {
    ngrok.connect(port, (innerErr, url) => {
      if (innerErr) {
        return logger.error(innerErr);
      }

      logger.appStarted(port, prettyHost, url);
    });
  } else {
    logger.appStarted(port, prettyHost);
  }
});
