/**
 *
 * App
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 */

import React from 'react';
import Helmet from 'react-helmet';
import styled from 'styled-components';
import { Link } from 'react-router';
import './style.scss';
import '../Checkout/style.scss';

const AppWrapper = styled.div`
  max-width: calc( 1170px * 2);
  margin: 0 auto;
  display: flex;
  min-height: 100%;
  padding: 0;
  flex-direction: column;
`;

export function App(props) {
  console.log(props)
  return (
    <AppWrapper>
      <Helmet
        titleTemplate="%s - React.js Boilerplate"
        defaultTitle="React.js Boilerplate"
        meta={[
          { name: 'description', content: 'A React.js Boilerplate application' },
        ]}
      />
      {
        !('splat' in props.params)
        ? <nav className="topmenu fixed">
            <h1 className="logo">
              <Link to="/" className="logo-small"><img src="https://placeholdit.imgix.net/~text?txtsize=15&txt=C&w=40&h=60" /></Link>
              <Link to="/" className="logo-default"><img src="http://placehold.it/220x60&text=CarService Corp" /></Link>
            </h1>
            <div className="search">
              <input type="text" defaultValue="Hi, what are u looking for?" className="input-large search-query" id="searchbox" />
              <a className="btn" href="search.html"><i className="fa fa-search"></i></a>
            </div>
            <div className="check-out-btn">
              <Link to="/checkout" className="btn checkout">To Checkout</Link>
            </div>
          </nav>

        : ''
      }
      <header className="header fixed" ></header>
      {React.Children.toArray(props.children)}
    </AppWrapper>
  );
}

App.propTypes = {
  children: React.PropTypes.node,
};

export default App;
