import { takeLatest, call, put } from 'redux-saga/effects';
import svc from '../../services/service.checkout';

import {
  GET_CART,
  GET_CART_END,
  GET_CART_ERR,
  GET_CART_OK,

  UPDATE_CART,
  UPDATE_CART_END,
  UPDATE_CART_ERR,
  UPDATE_CART_OK,

  DELETE_CART,
  DELETE_CART_END,
  DELETE_CART_ERR,
  DELETE_CART_OK,

  DELETE_ALL_CART,
  DELETE_ALL_CART_END,
  DELETE_ALL_CART_ERR,
  DELETE_ALL_CART_OK,

} from './constants';

function* getCart() {
  try {
    const xhr = yield call(svc.getCart);
    if (xhr.status > 200 || !xhr.success) {
      throw xhr;
    }
    yield put({ type: GET_CART_OK, xhr });
  } catch (e) {
    yield put({ type: GET_CART_ERR, e, errors: e.errors });
  } finally {
    yield put({ type: GET_CART_END });
  }
}

function* updateCart(action) {
  try {
    const xhr = yield call(svc.updateCart, action.data);
    if (xhr.status > 200 || !xhr.success) {
      throw xhr;
    }
    const state = action.state;
    yield put({ type: UPDATE_CART_OK, state, xhr });
  } catch (e) {
    yield put({ type: UPDATE_CART_ERR, e, errors: e.errors });
  } finally {
    yield put({ type: UPDATE_CART_END });
  }
}

function* deleteCart(action) {
  try {
    const xhr = yield call(svc.deleteCart, action.data.id);
    if (xhr.status > 200 || !xhr.success) {
      throw xhr;
    }

    const {state} = action.data
    yield put({ type: DELETE_CART_OK, xhr, state });
  } catch (e) {
    yield put({ type: DELETE_CART_ERR, e, errors: e.errors });
  } finally {
    yield put({ type: DELETE_CART_END });
  }
}

function* deleteAllCart(action) {
  try {
    const xhr = yield call(svc.deleteCart);
    if (xhr.status > 200 || !xhr.success) {
      throw xhr;
    }
    yield put({ type: DELETE_ALL_CART_OK, xhr });
  } catch (e) {
    yield put({ type: DELETE_ALL_CART_ERR, e, errors: e.errors });
  } finally {
    yield put({ type: DELETE_ALL_CART_END });
  }
}

// All export function Generator sagas

export function* getCartSaga() {
  yield takeLatest(GET_CART, getCart);
}

export function* postCartSaga() {
  yield takeLatest(POST_CART, postCart);
}

export function* updateCartSaga() {
  yield takeLatest(UPDATE_CART, updateCart);
}

export function* deleteCartSaga() {
  yield takeLatest(DELETE_CART, deleteCart);
}

export function* deleteAllCartSaga() {
  yield takeLatest(DELETE_ALL_CART, deleteAllCart);
}


// All sagas to be loaded
export default [
  getCartSaga,
  updateCartSaga,
  deleteCartSaga,
  deleteAllCartSaga,
];
