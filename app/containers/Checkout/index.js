/*
 *
 * Checkout
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import makeSelectCheckout from './selectors';
import { growl } from '../../utils/helpers';
import * as action from './actions';
import './check-out.scss';
import './style.scss';

export class Checkout extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);

    this.state = {
      items: [],
      itemsTotalAmount: null,
      currency: null,
    };

    this.is_retrieve_products = false;
    this.api_url = window.APP_CONFIG.api_host;
    this.itemExist = this.itemExist.bind(this);
    this.checkForm = this.checkForm.bind(this);
    this.onChangeAmount = this.onChangeAmount.bind(this);
    this.calculationCart = this.calculationCart.bind(this);
  }

  componentWillMount() {
    this.props.dispatch(action.getCart());
    setTimeout(() => { this.calculationCart(); }, 100);
  }

  componentDidUpdate(nextProps) {
    const { message, notify } = nextProps.Checkout;
    if (!!message && !!notify) {
      growl(message, notify, 10000);
    }
  }


  removeItem(item, index) {
    const state = this.props.Checkout;
    const newState = state;

    if (index > -1) {
      newState.cart.items.splice(index, 1);
    }

    const data = {
      state: newState,
      id: item.product.id,
    };
    this.props.dispatch(action.deleteCart(data));

    if (newState.cart.items.length === 0) {
      const data = {
        items: [],
        itemsTotalAmount: null,
        currency: null,
      };
      this.setState(data);
    } else {
      this.setState({ items: newState.cart.items });
    }
    setTimeout(() => { this.calculationCart(); }, 100);
  }

  itemExist(id) {
    const { cart } = this.props.Checkout;
    const list = cart.items || [];
    if (list.length > 0) {
      for (let i = 0; i < list.length; i++) {
        if (list[i].product.id === id) {
          return 'btn-addtocart inactive';
        }
      }
    }

    return 'btn-addtocart';
  }

  onChangeAmount(e, index) {
    if (e.target.value > 0 && e.target.value <= 100 ) {
      const newState = this.props.Checkout;
      let totalAmount = null;
      const price = newState.cart.items[index].product.prices[1].amount;

      newState.cart.items[index].quantity = e.target.value;
      totalAmount = e.target.value * price;
      newState.cart.items[index].totalAmount = totalAmount;

      // total of Items amount Calculation
      let itemsTotalAmount = null;
      const items = newState.cart.items;
      for (let i = items.length - 1; i >= 0; i--) {
        itemsTotalAmount = parseFloat(itemsTotalAmount + items[i].totalAmount);
      }

      itemsTotalAmount = parseFloat(itemsTotalAmount + newState.inVoiceFee);

      const vat = parseFloat(newState.vatFeeInPercent / 100).toFixed(2);
      const vatAmount = vat * itemsTotalAmount;
      itemsTotalAmount += vatAmount;
      const dataState = {
        items: newState.cart.items,
        itemsTotalAmount,
        currency: newState.cart.items[index].product.prices[1].currency,
      };

      this.setState(dataState);
      newState.itemsTotalAmount = dataState.itemsTotalAmount;
      newState.currency = dataState.currency;

      let data = {
        id: newState.cart.items[index].product.id,
        quantity:  newState.cart.items[index].quantity
      };
      this.props.dispatch(action.updateCart(newState, data));
    }
  }

  calculationCart() {
    const newState = this.props.Checkout;
    if (newState.cart.items.length > 0) {
      const items = newState.cart.items;
      let itemsTotalAmount = null;

      for (let i = items.length - 1; i >= 0; i--) {
        itemsTotalAmount = parseFloat(itemsTotalAmount + items[i].product.prices[1].amount);
        items[i].totalAmount = items[i].product.prices[1].amount * items[i].quantity;
      }

      newState.cart.items = items;

      itemsTotalAmount = parseFloat(itemsTotalAmount + newState.inVoiceFee);

      const vat = parseFloat(newState.vatFeeInPercent / 100).toFixed(2);
      const vatAmount = vat * itemsTotalAmount;

      itemsTotalAmount += vatAmount;
      const data = {
        items: newState.cart.items,
        itemsTotalAmount,
        currency: newState.cart.items[0].product.prices[1].currency,
      };

      this.setState(data);
      newState.itemsTotalAmount = data.itemsTotalAmount;
      newState.currency = data.currency;
      this.props.dispatch(action.updateState(newState));
    }
  }

  checkForm(form) {
    if (!form.terms.checked) {
      form.terms.focus();
      return false;
    }
    return true;
  }

  render() {
    let {cart, inputDefualtValue } = this.props.Checkout;
    const items = cart && cart.items || [];
    return (
      <div className="checkout-page container content-fixed">
        <div id="growl" />
        <section className="basket-list mgt20">
          <h1>Cart</h1>
          <ul>
            {
             (items && items.length > 0)
              ? items.map((item, index) => (
                <li className="basket-item flex-container" key={item.product.id}>
                  <div className="product-item-image flex-item">
                    <img src={`${this.api_url}${item.product.imageUrl}`} />
                  </div>
                  <div className="product-item-description flex-item">
                    <h2><a href="product.html">{item.product.title}</a></h2>
                    <a href="#">Description</a>
                  </div>
                  <div className="product-item-qtn flex-item">
                    <input
                      type="number"
                      defaultValue={item.quantity}
                      onChange={(e) => this.onChangeAmount(e, index)}
                      min="0" max="100"
                    /> pcs
                        <p><b>In Stock</b> 1-3 days</p>
                  </div>
                  <p className="product-item-price">{`x ${item.product.prices[1].amount} ${item.product.prices[1].currency}`}</p>
                  <p className="product-item-total">
                    = { this.state.items.length > 0 ? parseFloat(this.state.items[index].totalAmount).toFixed(2) : parseFloat(item.product.prices[1].amount).toFixed(2)}
                    {` ${item.product.prices[1].currency}`}
                  </p>
                  <p><span className="btn-remove" onClick={(e) => this.removeItem(item, index)}>Remove</span></p>
                </li>
                  ))
                : ''
              }
          </ul>
          <div className="basket-summary pull-right">
            <ul>
              <li>Invoice fee: {this.props.Checkout.inVoiceFee}{` ${this.state.currency ? this.state.currency : 'EUR'}`}</li>
              <li>T{'otal inc. vat '}
                {this.state.itemsTotalAmount ? parseFloat(this.state.itemsTotalAmount).toFixed(2) : 0}
                {` ${this.state.currency ? this.state.currency : 'EUR'}`}
              </li>
            </ul>
          </div>
        </section>
        ﻿<section className="delivery">
          <h1>Delivery</h1>
          <label className="disable"><input type="radio" disabled="" name="delivery" />Pickup at delivery-point (4,90 $)</label><br />
          <label className="disable"><input type="radio" disabled="" name="delivery" />Home delivery (29,90 $)</label><br />
          <label className="disable"><input type="radio" name="delivery" className="pull-left" />Pickup in store (4,90 $)</label><br />
          <div className="select-shop"><select><option>Choose store</option></select></div>
          <label className="disable"><input type="radio" className="pull-left" name="delivery" />Company delivery (10 $)</label><br />
          <span>Estimated deliverytime: <b>1-3 days</b></span>
        </section>
        <section className="delivery">
          <h1>Payment</h1>
          <label><input type="radio" defaultChecked name="payment" />Invoice {this.props.Checkout.inVoiceFee} EUR</label><br />
          <label><input type="radio" name="payment" />Paypal account (0 $)</label><br />
          <label><input type="radio" name="payment" />Visa or MasterCard (0kr)</label><br />
          <label><input type="radio" name="payment" />Bank Transfer (0kr)</label><br />
        </section>
        <section className="delivery">
          <h1>Contact information</h1>
          <div>
            <label><input type="radio" name="id" />Private</label>
            <label><input type="radio" name="id" />Company</label>
          </div>
          <form role="form" onSubmit={this.checkForm}>
            <div className="form-group row">
              <label htmlFor="inputEmail3" className="col-sm-2 control-label">Id number</label>
              <div className="col-sm-4">
                <input type="text" className="form-control" id="inputEmail3" />
              </div>
              <div className="col-sm-3">
                <a className="btn checkout" href="#">Get my address</a>
              </div>
            </div>
            <div className="form-group row">
              <label htmlFor="inputEmail3" className="col-sm-2 control-label">c/o</label>
              <div className="col-sm-4">
                <input type="text" className="form-control" id="inputEmail3" />
              </div>
            </div>
            <div className="form-group row">
              <label htmlFor="inputEmail3" className="col-sm-2 control-label">Firstname*</label>
              <div className="col-sm-4">
                <input type="text" className="form-control" id="inputEmail3" required />
              </div>
            </div>
            <div className="form-group row">
              <label htmlFor="inputEmail3" className="col-sm-2 control-label">Lastname*</label>
              <div className="col-sm-4">
                <input type="text" className="form-control" id="inputEmail3" required />
              </div>
            </div>
            <div className="form-group row">
              <label htmlFor="inputEmail3" className="col-sm-2 control-label">Address*</label>
              <div className="col-sm-4">
                <input type="text" className="form-control" id="inputEmail3" required />
              </div>
            </div>
            <div className="form-group row">
              <label htmlFor="inputEmail3" className="col-sm-2 control-label">Postalcode*</label>
              <div className="col-sm-4">
                <input type="text" className="form-control" id="inputEmail3" required />
              </div>
            </div>
            <div className="form-group row">
              <label htmlFor="inputEmail3" className="col-sm-2 control-label">Phone*</label>
              <div className="col-sm-4">
                <input type="text" className="form-control" id="inputEmail3" required />
              </div>
            </div>
            <div className="form-group row">
              <label htmlFor="inputEmail3" className="col-sm-2 control-label">Email*</label>
              <div className="col-sm-4">
                <input type="email" className="form-control" id="inputEmail3" required />
              </div>
            </div>
            <div className="form-group row">
              <label htmlFor="inputEmail3" className="col-sm-2 control-label">Confirm email*</label>
              <div className="col-sm-4">
                <input type="email" className="form-control" id="inputEmail3" required />
              </div>
            </div>
            <div><label><input type="checkbox" />Yes please, I want newsletters from CarService Corp</label></div>
            <div><i>* Mandatory fields</i></div>

            <p className="center"><label><input type="checkbox" name="terms" required />I accept CarService Corp's <a href="#">terms</a></label></p>
            <button type="submit" className="btn btn-large confirm">Send order</button>
          </form>
        </section>
      </div>
    );
  }
}

Checkout.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  Checkout: makeSelectCheckout(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Checkout);
