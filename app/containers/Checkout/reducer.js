/*
 *
 * Checkout reducer
 *
 */

import { fromJS } from 'immutable';
import {

  GET_CART,
  GET_CART_END,
  GET_CART_ERR,
  GET_CART_OK,

  UPDATE_CART,
  UPDATE_CART_END,
  UPDATE_CART_ERR,
  UPDATE_CART_OK,

  DELETE_CART,
  DELETE_CART_END,
  DELETE_CART_ERR,
  DELETE_CART_OK,

  DELETE_ALL_CART,
  DELETE_ALL_CART_END,
  DELETE_ALL_CART_ERR,
  DELETE_ALL_CART_OK,

  UPDATE_STATE,
} from './constants';

const initialState = fromJS({
  cart: {},
  error: undefined,
  errors: [],
  message: '',
  notify: false,
  success: undefined,
  inVoiceFee: 3,
  vatFeeInPercent: 20,
  itemsTotalAmount: null,
  currency: null,
  inputDefaultValue: 1
});

function checkoutReducer(state = initialState, action) {
  const xhr = action.xhr || null;
  switch (action.type) {
  // Action
    case GET_CART:
    case UPDATE_CART:
    case DELETE_CART:
    case DELETE_ALL_CART:
      return state;

    case UPDATE_STATE:
      return state
        .set('itemsTotalAmount', action.state.itemsTotalAmount)
        .set('currency', action.state.currency);

  // OK
    case GET_CART_OK:
      return state
        .set('cart', xhr);

    case UPDATE_CART_OK:
      return state
        .set('itemsTotalAmount', action.state.itemsTotalAmount)
        .set('cart', action.state.cart);

    case DELETE_CART_OK:
      return state
        .set('message', 'Successfully deleted item in cart.')
        .set('notify', 'notice')
        .set('cart', action.state.cart);

    case DELETE_ALL_CART_OK:
      return state
        .set('cart', {});
  // ERR
    case GET_CART_ERR:
    case UPDATE_CART_ERR:
    case DELETE_CART_ERR:
    case DELETE_ALL_CART_ERR:
      return state
        .set('error', xhr)
        .set('errors', []);

  // END
    case DELETE_CART_END:
    case GET_CART_END:
    case UPDATE_CART_END:
    case DELETE_ALL_CART_END:
    return state
      .set('message', '')
      .set('notify', false);

    default:
      return state;
  }
}

export default checkoutReducer;
