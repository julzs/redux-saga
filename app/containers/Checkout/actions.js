/*
 *
 * Checkout actions
 *
 */

import {
  GET_CART,
  UPDATE_CART,
  DELETE_CART,
  DELET_ALL_CART,
  UPDATE_STATE,
} from './constants';


export function getCart() {
  return { type: GET_CART };
}

export function updateCart(state, data) {
  return { type: UPDATE_CART, state, data };
}

export function deleteCart(data) {
  return { type: DELETE_CART, data };
}

export function deleteAllCart() {
  return { type: DELETE_ALL_CART };
}

export function updateState(state, data) {
  return { type: UPDATE_STATE, state, data};
}
