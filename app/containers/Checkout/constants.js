/*
 *
 * Checkout constants
 *
 */

export const GET_CART = 'GET_CART';
export const GET_CART_END = 'GET_CART_END';
export const GET_CART_ERR = 'GET_CART_ERR';
export const GET_CART_OK = 'GET_CART_OK';

export const UPDATE_CART = 'UPDATE_CART';
export const UPDATE_CART_END = 'UPDATE_CART_END';
export const UPDATE_CART_ERR = 'UPDATE_CART_ERR';
export const UPDATE_CART_OK = 'UPDATE_CART_OK';

export const DELETE_CART = 'DELETE_CART';
export const DELETE_CART_END = 'DELETE_CART_END';
export const DELETE_CART_ERR = 'DELETE_CART_ERR';
export const DELETE_CART_OK = 'DELETE_CART_OK';

export const DELETE_ALL_CART = 'DELETE_ALL_CART';
export const DELETE_ALL_CART_END = 'DELETE_ALL_CART_END';
export const DELETE_ALL_CART_ERR = 'DELETE_ALL_CART_ERR';
export const DELETE_ALL_CART_OK = 'DELETE_ALL_CART_OK';

export const UPDATE_STATE ='UPDATE_STATE';


