/*
 *
 * Products reducer
 *
 */

import { fromJS } from 'immutable';
import {
  GET_PRODUCTS,
  GET_PRODUCTS_END,
  GET_PRODUCTS_ERR,
  GET_PRODUCTS_OK,

  GET_PRODUCT,
  GET_PRODUCT_END,
  GET_PRODUCT_ERR,
  GET_PRODUCT_OK,

  GET_CART,
  GET_CART_END,
  GET_CART_ERR,
  GET_CART_OK,

  POST_CART,
  POST_CART_END,
  POST_CART_ERR,
  POST_CART_OK,

} from './constants';

const initialState = fromJS({
  cart: {},
  products: [],
  product: {},
  error: undefined,
  errors: [],
  message: '',
  notify: false,
  success: undefined,
});

function productsReducer(state = initialState, action) {
  const xhr = action.xhr || null;
  switch (action.type) {
  // Action
    case GET_PRODUCTS:
    case GET_PRODUCT:
    case POST_CART:
      return state;

  // OK
    case GET_PRODUCTS_OK:
      return state
        .set('products', xhr.data);

    case GET_PRODUCT_OK:
      return state
        .set('product', xhr);

    case POST_CART_OK:
      return state
        .set('message', 'Successfully added cart.')
        .set('notify', 'success')
        .set('cart', xhr);

  // ERR
    case GET_PRODUCTS_ERR:
      return state
        .set('error', true)
        .set('errors', [])
        .set('message', action.e.message)
        .set('notify', 'error');

    case GET_PRODUCT_ERR:
    case POST_CART_ERR:
      return state
        .set('error', xhr)
        .set('errors', []);

  // END
    case GET_PRODUCTS_END:
    case GET_PRODUCT_END:
    case POST_CART_END:
      return state
        .set('message', '')
        .set('notify', false);

    default:
      return state;
  }
}

export default productsReducer;
