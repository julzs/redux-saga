/*
 *
 * Products actions
 *
 */

import {
  GET_PRODUCTS,
  GET_PRODUCT,
  POST_CART,
} from './constants';

export function getProducts() {
  return { type: GET_PRODUCTS };
}

export function getProduct(id) {
  return { type: GET_PRODUCT, id };
}

export function postCart(data) {
  return { type: POST_CART, data };
}
