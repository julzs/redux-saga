/*
 *
 * Products constants
 *
 */

export const GET_PRODUCTS = 'GET_PRODUCTS';
export const GET_PRODUCTS_END = 'GET_PRODUCTS_END';
export const GET_PRODUCTS_ERR = 'GET_PRODUCTS_ERR';
export const GET_PRODUCTS_OK = 'GET_PRODUCTS_OK';

export const GET_PRODUCT = 'GET_PRODUCT';
export const GET_PRODUCT_END = 'GET_PRODUCT_END';
export const GET_PRODUCT_ERR = 'GET_PRODUCT_ERR';
export const GET_PRODUCT_OK = 'GET_PRODUCT_OK';

export const POST_CART = 'POST_CART';
export const POST_CART_END = 'POST_CART_END';
export const POST_CART_ERR = 'POST_CART_ERR';
export const POST_CART_OK = 'POST_CART_OK';

