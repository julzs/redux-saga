import { takeLatest, call, put } from 'redux-saga/effects';
import svc from '../../services/service.checkout';

import {
  GET_PRODUCTS,
  GET_PRODUCTS_END,
  GET_PRODUCTS_ERR,
  GET_PRODUCTS_OK,

  GET_PRODUCT,
  GET_PRODUCT_END,
  GET_PRODUCT_ERR,
  GET_PRODUCT_OK,

  POST_CART,
  POST_CART_END,
  POST_CART_ERR,
  POST_CART_OK,

} from './constants';

function* getProducts() {
  try {
    const xhr = yield call(svc.getProducts);
    if (xhr.status > 200 || !xhr.success) {
      throw xhr;
    }
    yield put({ type: GET_PRODUCTS_OK, xhr });
  } catch (e) {
    yield put({ type: GET_PRODUCTS_ERR, e, errors: e.errors });
  } finally {
    yield put({ type: GET_PRODUCTS_END });
  }
}

function* getProduct(action) {
  try {
    const xhr = yield call(svc.getProduct, action.id);
    if (xhr.status > 200 || !xhr.success) {
      throw xhr;
    }
    yield put({ type: GET_PRODUCT_OK, xhr });
  } catch (e) {
    yield put({ type: GET_PRODUCT_ERR, e, errors: e.errors });
  } finally {
    yield put({ type: GET_PRODUCT_END });
  }
}


function* postCart(action) {
  try {
    const xhr = yield call(svc.postCart, action.data);
    if (xhr.status > 200 || !xhr.success) {
      throw xhr;
    }
    yield put({ type: POST_CART_OK, xhr });
  } catch (e) {
    yield put({ type: POST_CART_ERR, e, errors: e.errors });
  } finally {
    yield put({ type: POST_CART_END });
  }
}

// All export function Generator sagas

export function* getProductsSaga() {
  yield takeLatest(GET_PRODUCTS, getProducts);
}

export function* getProductSaga() {
  yield takeLatest(GET_PRODUCT, getProduct);
}

export function* postCartSaga() {
  yield takeLatest(POST_CART, postCart);
}


// All sagas to be loaded
export default [
  getProductsSaga,
  getProductSaga,
  postCartSaga,
];
