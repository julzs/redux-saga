/*
 *
 * Products
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import makeSelectProducts from './selectors';
import * as action from './actions';
import { forwardTo, growl } from '../../utils/helpers';
import './style.scss';

export class Products extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);

    this.is_retrieve_products = false;
    this.api_url = window.APP_CONFIG.api_host;
  }

  componentWillMount() {
    this.props.dispatch(action.getProducts());
  }

  componentDidUpdate(nextProps) {
    const { message, notify } = nextProps.Products;
    if (!!message && !!notify) {
      growl(message, notify, 10000);
    }
  }

  addToCart(e, product) {
    e.preventDefault();
    this.props.dispatch(action.postCart({id:product.id, quantity: 1 }));
  }

  render() {
    let { products } = this.props.Products;
    products = products || [];
    return (
      <div className="checkout-page container content-fixed">
        <div id="growl" />
        <section className="basket-list">
          <h1>Products</h1>
          <ul>
            {
              (products.length > 0)
              ? products.map((product) => (
                <li className="basket-item flex-container" key={product.id}>
                  <div className="product-item-image flex-item">
                    <img src={`${this.api_url}${product.imageUrl}`} />
                  </div>
                  <div className="product-item-description flex-item">
                    <h2><a href="product.html">{product.title}</a></h2>
                    <a href="#">Description</a>
                  </div>
                  <div className="product-item-qtn flex-item">
                    <p><b>In Stock</b> 1-3 days</p>
                  </div>
                  <p className="product-item-price">{`${product.prices[1].amount} ${product.prices[1].currency}`}</p>
                  <p><span className="btn-addtocart" onClick={(e) => this.addToCart(e, product)}>Add to Cart</span></p>
                </li>
                  ))
              : ''
            }
          </ul>
        </section>
      </div>
    );
  }
}

Products.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  Products: makeSelectProducts(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Products);
