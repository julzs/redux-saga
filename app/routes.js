// These are the pages you can go to.
// They are all wrapped in the App component, which should contain the navbar etc
// See http://blog.mxstbr.com/2016/01/react-apps-with-pages for more information
// about the code splitting business
import { getAsyncInjectors } from 'utils/asyncInjectors';

const errorLoading = (err) => {
  console.error('Dynamic page loading failed', err); // eslint-disable-line no-console
};

const loadModule = (cb) => (componentModule) => {
  cb(null, componentModule.default);
};

window.loaded_screens = [];

export default function createRoutes(store) {
  // Create reusable async injectors using getAsyncInjectors factory
  const { injectReducer, injectSagas } = getAsyncInjectors(store); // eslint-disable-line no-unused-vars

  const getModules = (component) => {
    const modules = [];

    if (window.loaded_screens.indexOf(component.name) < 0) {
      window.loaded_screens.push(component.name);

      if (!component.skip_reducer) {
        modules.push(import(`containers/${component.container}/reducer`));
      }

      if (!component.skip_saga) {
        modules.push(import(`containers/${component.container}/sagas`));
      }

      if (!!component.reducers) {
        for (let i = 0; i < component.reducers.length; i++) {
          // Load additional reducers
          modules.push(import(`containers/${component.reducers[i]}/reducer`));
        }
      }
    }
    return modules;
  };


return [
    {
      path: '/',
      name: 'products',
      getComponent(nextState, cb) {
        const modules = getModules({
          name: this.name,
          container: 'Products',
        });
        modules.push(import('containers/Products'));

        const importModules = Promise.all(modules);

        const renderRoute = loadModule(cb);

        importModules.then((results) => {
          if (results.length > 1) {
            injectReducer('products', results[0].default);
            injectSagas(results[1].default);
          }
          setTimeout(() => { renderRoute(results[results.length - 1]); }, 100);
        });

        importModules.catch(errorLoading);
      }
    },
    {
      path: 'checkout',
      name: 'checkout',
      getComponent(nextState, cb) {
        const modules = getModules({
          name: this.name,
          container: 'Checkout',
        });
        modules.push(import('containers/Checkout'));

        const importModules = Promise.all(modules);

        const renderRoute = loadModule(cb);

        importModules.then((results) => {
          if (results.length > 1) {
            injectReducer('checkout', results[0].default);
            injectSagas(results[1].default);
          }
          setTimeout(() => { renderRoute(results[results.length - 1]); }, 100);
        });

        importModules.catch(errorLoading);
      },
    },
    {
      path: '*',
      name: 'notfound',
      getComponent(nextState, cb) {
        import('containers/NotFoundPage')
          .then(loadModule(cb))
          .catch(errorLoading);
      },
    },
  ];
}
