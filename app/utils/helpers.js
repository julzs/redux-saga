
export function growl(message, message_type, ttl = 5000, onClose = () => {}) {
  if (!window.NotificationFx) return false;

  const NotificationFx = window.NotificationFx;

  if (global.notification) {
    global.notification.remove();
  }

  if (!!message && !!message_type) {
    global.notification = new NotificationFx({
      effect: 'jelly',
      layout: 'bar', // growl|attached|bar|other
      message: `<p>${message}</p>`,
      onClose,
      type: message_type, // notice, warning, error or success,
      ttl,
      wrapper: document.getElementById('growl'),
    });
    global.notification.show();
  }

  return true;
}

export function renderFieldErrors(errors, field) {
  if (!errors) {
    return '';
  }
  const React = require('react');
  const dom = [];

  for (let i = 0; !!errors[field] && i < errors[field].length; i++) {
    dom.push(<span className="error" key={i}>{errors[field][i]}</span>);
  }

  return dom.length > 0 ? dom : '';
}

export function renderFormMessages(messages, type = 'error') {
  if (!type) return '';

  const React = require('react');
  const dom = [];
  for (let i = 0; !!messages && i < messages.length; i++) {
    dom.push(<li className="form-message" key={i}>{messages[i]}</li>);
  }

  return dom.length > 0 ? <div className={`${type}-alert`}><ul>{dom}</ul></div> : '';
}

