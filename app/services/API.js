import {
  ACCESS_401,
  ACCESS_403,
} from '../constants';

let env;
const headers = {
  'Content-Type': 'application/json',
  'Cache-Control': 'no-cache',
  'max-age': 0,
};

if (!!process.env.NODE_ENV && ['local', 'test'].indexOf(process.env.NODE_ENV) < 0) {
  env = {
    api: {
      baseURL: window.APP_CONFIG ? window.APP_CONFIG.api_host : process.env.BASE_URL,
    },
  };
  if (document.getElementById('API_URL')) {
    env.api.baseURL = document.getElementById('API_URL').content;
  }
}

class API {
  constructor() {
    this.sending = false;
    this.host = window.APP_CONFIG ? window.APP_CONFIG.api_host : env.api.baseURL;
    this.headers = headers;
    this._fullURL = this._fullURL.bind(this);

    this.post = this.post.bind(this);
    this.url = this._fullURL();
    this.xhr = new XMLHttpRequest();

    this.routes = window.API_ROUTES || {};
  }

  _fullURL(path) {
    return `${this.host}${path || ''}`;
  }

  _handle4XX(json_response, status_code) {
    switch (status_code) {
      case 401:
        return ACCESS_401;
      case 403:
        return ACCESS_403;
      default:
        return status_code;
    }
  }

  setURL(path) {
    this.url = this._fullURL(path);
  }

  send(method, path, params, file) {
    return new Promise((resolve, reject) => { // eslint-disable-line no-unused-vars
      let data;
      let content_type = 'application/json;charset=UTF-8';
      if (this.sending) {
        this.xhr.abort();
        this.sending = false;
      }

      this.setURL(path);
      if (!!params && method === 'GET') {
        const s_qry = Object.keys(params).map(
          (k) => (`${encodeURIComponent(k)}=${encodeURIComponent(params[k])}`)
        ).join('&');

        this.setURL(`${path}?${s_qry}`);
      } else if (file) {
        content_type = '';
        data = new FormData();
        data.append('document', file);
        // File upload
      } else if (params) {
        data = JSON.stringify(params);
      }

      if ('withCredentials' in this.xhr) {
        this.xhr.open(method, this.url, true);
      } else if (typeof XDomainRequest !== 'undefined') {
        this.xhr = new XDomainRequest();
        this.xhr.open(method, this.url);
      } else {
        this.xhr.open(method, this.url);
      }

      if (content_type) {
        this.xhr.setRequestHeader('Content-Type', content_type);
      }

      this.xhr.setRequestHeader('Cache-Control', 'no-cache');
      this.xhr.onload = () => {
        this.sending = false;

        if (!this.xhr.response) {
          return resolve({ success: false, status: this.xhr.status, message: 'Server error encountered.  Please try again later.' });
        }

        try {
          const json_response = JSON.parse(this.xhr.response);
          let success = json_response.success || json_response.Success;
          if (json_response.hasOwnProperty('Success')) {
            delete json_response.Success;
            json_response.success = success;
          }

          if (this.xhr.status % 400 < 100) {
            json_response.message = json_response.message || json_response.error;
            json_response.error_type = this._handle4XX(json_response, this.xhr.status);
          }

          if (this.xhr.status >= 500) {
            success = false;
            resolve({
              ...json_response,
              success: false,
              status: this.xhr.status,
              message: 'Server error encountered.  Please try again later.',
            });
          }

          if (json_response.data) {
            for (const key in json_response.data) { // eslint-disable-line no-restricted-syntax
              if (json_response.data.hasOwnProperty(key) && !!json_response.data[key]) {
                if (key.length > 3 && key.substr(-3) === '_id') {
                  const object_key = key.substr(0, key.length - 3);
                  json_response.data[object_key] = json_response.data[key];
                  if (json_response.data[object_key]._id) {
                    json_response.data[key] = json_response.data[object_key]._id;
                  }
                }
              }
            }
          }

          if (json_response instanceof Array) {
            return resolve({ data: json_response, status: this.xhr.status, success: true });
          }

          return resolve({ ...json_response, status: this.xhr.status, success: true });
        } catch (e) {
          return resolve({ success: false, status: this.xhr.status, e, message: 'Server error encountered.  Please try again later.' });
        }
      };

      this.xhr.onerror = (e) => {
        this.sending = false;

        if (!this.xhr.status) {
          resolve({ success: false, message: 'Server error encountered.  Please try again later.', stack: e });
        } else if (this.xhr.response) {
          const json_resp = JSON.parse(this.xhr.response);
          resolve({ ...json_resp, status: this.xhr.status, stack: e });
        } else {
          resolve({ success: false, message: 'Server error encountered.  Please try again later.', stack: e });
        }
      };

      this.sending = true;
      try {
        if (data) {
          this.xhr.send(data);
        } else {
          this.xhr.send();
        }
      } catch (e) {
        console.log(e); // eslint-disable-line no-console
        resolve(null);
      }
    });
  }

  get(path, params) {
    return this.send('GET', path, params);
  }

  post(path, data = false, file = false) {
    return this.send('POST', path, data, file);
  }

  patch(path, data = false, file = false) {
    return this.send('PATCH', path, data, file);
  }

  put(path, data = false, file = false) {
    return this.send('PUT', path, data, file);
  }

  delete(path, data = false) {
    return this.send('DELETE', path, data);
  }
}

export default API;
