import API from './API';

const svc = {

  getProducts() {
    return new API().get('/Products');
  },

  getProduct(id) {
    return new API().get(`Product/${id}`);
  },

  getCart() {
    return new API().get('/cart');
  },

  postCart(data) {
    return new API().post(`/cart/${data.id}?quantity=${data.quantity}`);
  },

  updateCart(data) {
    return new API().put(`/cart/${data.id}`, {quantity: data.quantity});
  },

  deleteCart(id) {
    return new API().delete(`/cart/${id}`);
  },
};

export default svc;
